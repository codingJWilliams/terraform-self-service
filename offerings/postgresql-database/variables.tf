variable "postgres_host" {
  type = string
  description = "PostgreSQL server hostname"
}

variable "postgres_port" {
  type = string
  description = "PostgreSQL server port"
}

variable "postgres_database" {
  type = string
  description = "PostgreSQL user database"
}

variable "postgres_username" {
  type = string
  description = "PostgreSQL user username"
}

variable "postgres_password" {
  type = string
  description = "PostgreSQL user password"
}

variable "USER_database_name" {
  type = string
  description = "Database to create within PostgreSQL. This name will be prefixed with a random string."
}