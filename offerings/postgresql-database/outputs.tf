output "name" {
    sensitive = true
    value = postgresql_database.db.name
}
output "password" {
    sensitive = true
    value = random_password.password.result
}