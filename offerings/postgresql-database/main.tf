terraform {
  required_providers {
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.22.0"
    }
  }
}

provider "postgresql" {
  host            = var.postgres_host
  port            = var.postgres_port
  database        = var.postgres_database
  username        = var.postgres_username
  password        = var.postgres_password
  sslmode         = "disable"
  connect_timeout = 15
}

resource "random_password" "prefix" {
  special = false
  upper = false
  length = 6
}

locals {
  db_name = "ssdb-${random_password.prefix.result}-${var.USER_database_name}"
}

resource "postgresql_role" "role" {
  name     = local.db_name
  login    = true
  password = random_password.password.result
}

resource "random_password" "password" {
  length  = 24
  special = false
}

resource "postgresql_database" "db" {
  name              = local.db_name
  owner             = postgresql_role.role.name
  connection_limit  = -1
  allow_connections = true
}

resource "postgresql_grant" "read_insert_column" {
  database    = postgresql_database.db.name
  role        = postgresql_role.role.name
  schema      = "public"
  object_type = "database"
  privileges  = ["CONNECT", "CREATE", "TEMPORARY"]
}