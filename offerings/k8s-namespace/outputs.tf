output "name" {
    sensitive = true
    value = kubernetes_namespace_v1.namespace.id
}