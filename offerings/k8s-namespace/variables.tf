variable "k8s_token" {
  type = string
  description = "Kubernetes admin token"
}
variable "k8s_host" {
  type = string
  description = "Kubernetes admin host"
}
variable "k8s_ca" {
  type = string
  description = "Kubernetes admin ca"
}

variable "USER_name" {
  type = string
  description = "Name of the namespace to create."
  validation {
    condition     = can(regex("^[0-9A-Za-z]+$", var.USER_name))
    error_message = "No special chars"
  }
}

variable "USER_service_account_name" {
  type = string
  description = "Service account name"
}

variable "USER_service_account_namespace" {
  type = string
  description = "Service account namespace"
}