terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.26.0"
    }
  }
}

locals {
  name = "db-${var.USER_name}"
}

provider "kubernetes" {
  token = var.k8s_token
  host = var.k8s_host
  cluster_ca_certificate = base64decode(var.k8s_ca)
}

resource "kubernetes_namespace_v1" "namespace" {
  metadata {
    name = local.name

    labels = {
      namespace = local.name
    }
  }
}

resource "kubernetes_network_policy_v1" "default-ingress" {
  metadata {
    name = "default-ingress"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  spec {
    pod_selector {
      
    }
    policy_types = ["Ingress"]
    ingress {
      from {
        namespace_selector {
          match_labels = {
            namespace = local.name
          }
        }
      }
    }
  }
}

resource "kubernetes_role_v1" "service-account-user" {
  metadata {
    name      = "user-sa-role"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  rule {
    api_groups     = ["", "extensions", "apps"]
    resources      = ["*"]
    resource_names = ["*"]

    verbs = ["*"]
  }
  rule {
    api_groups     = ["batch"]
    resources      = ["jobs", "cronjobs"]
    resource_names = ["*"]

    verbs = ["*"]
  }
}

resource "kubernetes_role_binding_v1" "service-account-role-binding" {
  metadata {
    name      = "user-sa-role-binding"
    namespace = kubernetes_namespace_v1.namespace.id
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role_v1.service-account-user.metadata[0].name
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = data.kubernetes_service_account_v1.service-account.metadata[0].name
    namespace = data.kubernetes_service_account_v1.service-account.metadata[0].namespace
  }
}

data "kubernetes_service_account_v1" "service-account" {
  metadata {
    name = var.USER_service_account_name
    namespace = var.USER_service_account_namespace
  }
}