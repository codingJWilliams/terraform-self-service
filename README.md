# Terraform Self Service

This project is a small Python API which allows untrusted users to execute Terraform in a trusted environment, passing parameters. This allows users to easily self-serve resources. 

For example, a user can make a POST request to the /deployments endpoint like follows:

```json
POST /deployments HTTP/1.1
Authorization: Bearer <token>

{
    "offering_id": "1",
    "user_vars": {"database_name": "myproject"},
    "comment": "Database for my project"
}
```

This will then create a Terraform workspace, and execute a terraform apply for the postgresql-database offering, and return back the outputs of the terraform module:

```json
{
    "comment": "Database for my project",
    "id": 12,
    "outputs": {
        "name": {
            "sensitive": true,
            "type": "string",
            "value": "ssdb-ouj2m4-myproject"
        },
        "password": {
            "sensitive": true,
            "type": "string",
            "value": "RaNd0mPa55W0Rd"
        }
    },
    "user_vars": {
        "database_name" = "dbnames"
    },
    "workspace_id": "74ea9459-1213-43ec-b0fc-32841f1f5b7c"
}
```

The API can be used to list deployments, delete or update existing deployments (which will terraform apply & destroy the resources, respectively), 