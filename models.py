from tortoise.models import Model
from tortoise import fields
from tortoise.contrib.pydantic import pydantic_model_creator
import os
from typing import Dict, List, Union
import subprocess
import json
import shutil
import secrets
import uuid

class Offering(Model):
    id = fields.IntField(pk = True)
    name = fields.CharField(max_length=255)
    system_vars = fields.JSONField()
    user_vars = fields.JSONField()
    deployments: fields.ReverseRelation["Deployment"]

gen_token = lambda: secrets.token_hex(64)

class User(Model):
    id = fields.IntField(pk = True)
    name = fields.CharField(max_length=255)
    administrator = fields.BooleanField(default = False)
    token = fields.CharField(max_length=128, default = gen_token)
    deployments: fields.ReverseRelation["Deployment"]

class Deployment(Model):
    id = fields.IntField(pk=True)
    workspace_id = fields.UUIDField(default=uuid.uuid4)
    comment = fields.TextField()
    state = fields.TextField(null = True)
    outputs = fields.JSONField(null = True)
    user_vars = fields.JSONField()
    offering: fields.ForeignKeyRelation[Offering] = fields.ForeignKeyField(
        "models.Offering", related_name="offering"
    )
    owner: fields.ForeignKeyRelation[User] = fields.ForeignKeyField(
        "models.User", related_name="owner"
    )


    async def terraform_apply(self, command="apply"):
        deployment_env = {
            "TF_WORKSPACE": str(self.id)
        }
        offering = await self.offering.first()
        deployment_path = os.path.join(os.curdir, "offerings", offering.name)
        state_dir = os.path.join(deployment_path, "terraform.tfstate.d", str(self.id))
        state_path = os.path.join(state_dir, "terraform.tfstate")

        os.makedirs(state_dir)
        if self.state != None and self.state != "":
            with open(state_path, "w") as f:
                f.write(self.state)

        for k in offering.system_vars:
            deployment_env["TF_VAR_" + k] = offering.system_vars[k]

        for k in offering.user_vars:
            deployment_env["TF_VAR_USER_" + k] = self.user_vars[k]


        result = subprocess.run(["/opt/homebrew/bin/terraform", "init", "-input=false"], env=deployment_env, cwd=deployment_path, capture_output=True)
        if result.returncode != 0:
            raise Exception(result.stderr + result.stdout)

        result = subprocess.run(["/opt/homebrew/bin/terraform", command, "-input=false", "-auto-approve"], env=deployment_env, cwd=deployment_path, capture_output=True)
        if result.returncode != 0:
            raise Exception(result.stderr + result.stdout)

        outputs = subprocess.run(["/opt/homebrew/bin/terraform", "output", "-json"], env=deployment_env, cwd=deployment_path, capture_output=True)
        if outputs.returncode != 0:
            raise Exception(outputs.stderr + outputs.stdout)
        
        self.outputs = json.loads(outputs.stdout)
        with open(state_path) as f:
            self.state = f.read()

        shutil.rmtree(state_dir)

    def __str__(self):
        return f"Deployment {self.id}"

BaseDeployment_Pydantic = pydantic_model_creator(Deployment, name="Deployment", exclude=['state'])
BaseDeploymentIn_Pydantic = pydantic_model_creator(Deployment, name="DeploymentIn", exclude_readonly=True, exclude=['state', 'outputs', 'workspace_id'], computed=['offering_id'])
class DeploymentIn_Pydantic(BaseDeploymentIn_Pydantic):
    user_vars: Dict[str, str]
    offering_id: int
class Deployment_Pydantic(BaseDeployment_Pydantic):
    user_vars: Union[Dict[str, str], None]
    outputs: Union[Dict[str, dict], None]

BaseOffering_Pydantic = pydantic_model_creator(Offering, name="Offering")
class Offering_Pydantic(BaseOffering_Pydantic):
    user_vars: List[str]
    system_vars: Dict[str, str]
OfferingIn_Pydantic = pydantic_model_creator(Offering, name="OfferingIn", exclude_readonly=True)

User_Pydantic = pydantic_model_creator(User, name="User")
UserIn_Pydantic = pydantic_model_creator(User, name="UserIn", exclude_readonly=True)
