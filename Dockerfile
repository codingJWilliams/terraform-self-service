FROM python:3.12

ENV TINI_VERSION="v0.19.0"

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

RUN pip install -U \
    pip \
    setuptools \
    wheel

RUN wget -O terraform.zip "https://releases.hashicorp.com/terraform/1.7.5/terraform_1.7.5_linux_amd64.zip" && unzip terraform.zip && rm -rf terraform.zip && mkdir -p /opt/homebrew/bin/ && mv terraform /opt/homebrew/bin

WORKDIR /app

RUN useradd -m -r user && \
    chown user /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

USER user

ENTRYPOINT ["/tini", "--"]
CMD ["/bin/sh", "entrypoint.sh"]