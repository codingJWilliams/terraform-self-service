from typing import Dict, List
from pydantic import BaseModel
from fastapi import FastAPI, Depends, Header
from fastapi.security import HTTPBearer
import settings
from starlette import status
from tortoise.contrib.fastapi import register_tortoise
from models import Deployment_Pydantic, DeploymentIn_Pydantic, Deployment, Offering, Offering_Pydantic, OfferingIn_Pydantic, User, User_Pydantic, UserIn_Pydantic
from starlette.exceptions import HTTPException

app = FastAPI(
    title = "Terraform Self Service",
    description = "Allows users to create resources which require high or root level priviledges to create, in a self service fashion.",
    openapi_tags=[
        {"name": "Deployments", "description": "A deployment is a specific instance of an Offering - for example, a deployment may contain the 'my-app' PostgreSQL database"},
        {"name": "Offerings", "description": "An offering is a type of terraform project which can be deployed to the cluster."},
        {"name": "Users", "description": "Users are individual API tokens which can manage their own deployments. Administrators can also manage offerings"},
    ]
)
security = HTTPBearer()


class UnauthorizedMessage(BaseModel):
    detail: str = "Bearer token missing or unknown"

async def get_auth_user(
    authorization: str = Depends(security),
) -> str:
    # _, token = authorization.split(" ")
    token = authorization.credentials
    user = await User.get(token = token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=UnauthorizedMessage().detail,
        )

    return user


# {
#   "name": "postgresql-database",
#   "system_vars": {
#             "postgres_host": "postgres.service.wtbt.uk",
#             "postgres_port": "5432",
#             "postgres_database": "postgres",
#             "postgres_username": "postgres",
#             "postgres_password": "w8Bjtx,YvintMzx2E_v2"
#         },
#   "user_vars": ["database_name"]
# }

class Status(BaseModel):
    message: str


class DeploymentCreateRequest(BaseModel):
    offering: str
    vars: Dict[str, str]

@app.get("/deployments", response_model=List[Deployment_Pydantic], tags=["Deployments"], openapi_extra={"x-speakeasy-entity-operation": "Deployment#list"})
async def get_deployments(user: User = Depends(get_auth_user)):
    return await Deployment_Pydantic.from_queryset(
        Deployment.all()
        if user.administrator else
        Deployment.filter(owner_id=user.id)
    )


@app.post("/deployments", response_model=Deployment_Pydantic, tags=["Deployments"], openapi_extra={"x-speakeasy-entity-operation": "Deployment#create"})
async def create_deployment(deployment: DeploymentIn_Pydantic, user: User = Depends(get_auth_user)):
    offering = await Offering.get(id=deployment.offering_id)
    for k in offering.user_vars:
        if k not in deployment.user_vars: raise HTTPException(400, "Missing user variable: " + k)

    deployment_obj = await Deployment.create(**deployment.model_dump(exclude_unset=True), owner_id=user.id)
    await deployment_obj.terraform_apply()
    await deployment_obj.save()
    return await Deployment_Pydantic.from_tortoise_orm(deployment_obj)


@app.get("/deployment/{deployment_id}", response_model=Deployment_Pydantic, tags=["Deployments"], openapi_extra={"x-speakeasy-entity-operation": "Deployment#read"})
async def get_deployment(deployment_id: str, user: User = Depends(get_auth_user)):
    return await Deployment_Pydantic.from_queryset_single(
        Deployment.get(id=deployment_id)
        if user.administrator else
        Deployment.get(id=deployment_id, owner_id=user.id)
    )


@app.put("/deployment/{deployment_id}", response_model=Deployment_Pydantic, tags=["Deployments"], openapi_extra={"x-speakeasy-entity-operation": "Deployment#update"})
async def update_deployment(deployment_id: str, deployment: DeploymentIn_Pydantic, user: User = Depends(get_auth_user)):
    if user.administrator: await Deployment.filter(id=deployment_id).update(**deployment.model_dump(exclude_unset=True))
    else: await Deployment.filter(id=deployment_id, owner_id=user.id).update(**deployment.model_dump(exclude_unset=True))

    deployment_obj = (await Deployment.get(id=deployment_id)) if user.administrator else (await Deployment.get(id=deployment_id, owner_id=user.id))
    await deployment_obj.terraform_apply()
    await deployment_obj.save()
    return await Deployment_Pydantic.from_tortoise_orm(deployment_obj)


@app.delete("/deployment/{deployment_id}", response_model=Status, tags=["Deployments"], openapi_extra={"x-speakeasy-entity-operation": "Deployment#delete"})
async def delete_deployment(deployment_id: str, user: User = Depends(get_auth_user)):
    to_delete = await (Deployment.filter(id=deployment_id) if user.administrator else Deployment.filter(id=deployment_id, owner_id=user.id)).get()
    await to_delete.terraform_apply("destroy")
    await to_delete.save()
    await to_delete.delete()
    return Status(message=f"Deleted deployment {deployment_id}")


@app.get("/offerings", response_model=List[Offering_Pydantic], tags=["Offerings"], openapi_extra={"x-speakeasy-entity-operation": "Offering#list"})
async def get_offerings(user: User = Depends(get_auth_user)):
    res = []
    for offering in await Offering_Pydantic.from_queryset(Offering.all()):
        offering = offering.dict()
        if not user.administrator:
            offering['system_vars'] = {}
        res += [offering]
    return res


@app.post("/offerings", response_model=Offering_Pydantic, tags=["Offerings"], openapi_extra={"x-speakeasy-entity-operation": "Offering#create"})
async def create_offering(offering: OfferingIn_Pydantic, user: User = Depends(get_auth_user)):
    if not user.administrator: raise HTTPException(status_code=403, detail="Only administrators may create offerings")
    offering_obj = await Offering.create(**offering.model_dump(exclude_unset=True))
    return await Offering_Pydantic.from_tortoise_orm(offering_obj)


@app.get("/offering/{offering_id}", response_model=Offering_Pydantic, tags=["Offerings"], openapi_extra={"x-speakeasy-entity-operation": "Offering#read"})
async def get_offering(offering_id: str, user: User = Depends(get_auth_user)):
    if user.administrator: return await Offering_Pydantic.from_queryset_single(Offering.get(id=offering_id))
    else: 
        offering = (await Offering_Pydantic.from_queryset_single(Offering.get(id=offering_id))).dict(exclude={'system_vars'})
        offering['system_vars'] = {}
        return offering


@app.put("/offering/{offering_id}", response_model=Offering_Pydantic, tags=["Offerings"], openapi_extra={"x-speakeasy-entity-operation": "Offering#update"})
async def update_offering(offering_id: str, offering: OfferingIn_Pydantic, user: User = Depends(get_auth_user)):
    if not user.administrator: raise HTTPException(status_code=403, detail="Only administrators may edit offerings")
    await Offering.filter(id=offering_id).update(**offering.model_dump(exclude_unset=True))
    return await Offering_Pydantic.from_queryset_single(Offering.get(id=offering_id))


@app.delete("/offering/{offering_id}", response_model=Status, tags=["Offerings"], openapi_extra={"x-speakeasy-entity-operation": "Offering#delete"})
async def delete_offering(offering_id: str, user: User = Depends(get_auth_user)):
    if not user.administrator: raise HTTPException(status_code=403, detail="Only administrators may delete offerings")
    deleted_count = await Offering.filter(id=offering_id).delete()
    if not deleted_count:
        raise HTTPException(status_code=404, detail=f"Offering {offering_id} not found")
    return Status(message=f"Deleted offering {offering_id}")

@app.get("/users", response_model=List[User_Pydantic], tags=["Users"])
async def get_users(user: User = Depends(get_auth_user)):
    if not user.administrator: raise HTTPException(status_code=403, detail="Only administrators may list users")
    return await User_Pydantic.from_queryset(User.all())


@app.post("/users", response_model=User_Pydantic, tags=["Users"])
async def create_user(user: UserIn_Pydantic, auth_user: User = Depends(get_auth_user)):
    if not auth_user.administrator: raise HTTPException(status_code=403, detail="Only administrators may create users")
    user_obj = await User.create(**user.model_dump(exclude_unset=True))
    return await User_Pydantic.from_tortoise_orm(user_obj)


@app.get("/user/{user_id}", response_model=User_Pydantic, tags=["Users"])
async def get_user(user_id: str, user: User = Depends(get_auth_user)):
    if not user.administrator: raise HTTPException(status_code=403, detail="Only administrators may get users")
    return await User_Pydantic.from_queryset_single(User.get(id=user_id))


@app.put("/user/{user_id}", response_model=User_Pydantic, tags=["Users"])
async def update_user(user_id: str, user: UserIn_Pydantic, auth_user: User = Depends(get_auth_user)):
    if not auth_user.administrator: raise HTTPException(status_code=403, detail="Only administrators may update users")
    await User.filter(id=user_id).update(**user.model_dump(exclude_unset=True))
    return await User_Pydantic.from_queryset_single(User.get(id=user_id))


@app.delete("/user/{user_id}", response_model=Status, tags=["Users"])
async def delete_user(user_id: str, user: User = Depends(get_auth_user)):
    if not user.administrator: raise HTTPException(status_code=403, detail="Only administrators may delete users")
    deleted_count = await User.filter(id=user_id).delete()
    if not deleted_count:
        raise HTTPException(status_code=404, detail=f"User {user_id} not found")
    return Status(message=f"Deleted user {user_id}")


register_tortoise(
    app,
    db_url=settings.TORTOISE_ORM['connections']['default'],
    modules={"models": ["models", "aerich.models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)
