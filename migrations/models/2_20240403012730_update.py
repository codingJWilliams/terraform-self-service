from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "deployment" ADD "owner_id" INT NOT NULL;
        CREATE TABLE IF NOT EXISTS "user" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "administrator" BOOL NOT NULL  DEFAULT False,
    "token" VARCHAR(128) NOT NULL
);
        ALTER TABLE "deployment" ADD CONSTRAINT "fk_deployme_user_b8709177" FOREIGN KEY ("owner_id") REFERENCES "user" ("id") ON DELETE CASCADE;"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "deployment" DROP CONSTRAINT "fk_deployme_user_b8709177";
        ALTER TABLE "deployment" DROP COLUMN "owner_id";
        DROP TABLE IF EXISTS "user";"""
