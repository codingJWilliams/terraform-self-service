from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        CREATE TABLE IF NOT EXISTS "offering" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "system_vars" JSONB NOT NULL,
    "user_vars" JSONB NOT NULL
);
CREATE TABLE IF NOT EXISTS "deployment" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "workspace_id" UUID NOT NULL,
    "comment" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "outputs" JSONB NOT NULL,
    "user_vars" JSONB NOT NULL,
    "offering_id" INT NOT NULL REFERENCES "offering" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(100) NOT NULL,
    "content" JSONB NOT NULL
);"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        """
