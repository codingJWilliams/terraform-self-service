from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "deployment" ALTER COLUMN "state" DROP NOT NULL;
        ALTER TABLE "deployment" ALTER COLUMN "outputs" DROP NOT NULL;"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "deployment" ALTER COLUMN "state" SET NOT NULL;
        ALTER TABLE "deployment" ALTER COLUMN "outputs" SET NOT NULL;"""
